taxCalApp.controller('cricket_app_controller', ['$scope', '$filter', function($scope, $filter) {

    
    function getRandomValue(max, min){
        return Math.floor(Math.random() * (max - min))  + min;
    }

    $scope.players = {
       "1": {
        "name" : "Jadega",
        "matches" : 3,
        "match_details" : {
            "1" : {
                "type" : 'Batsman',
                "runs" : getRandomValue(100, 30),
                "wicket" : getRandomValue(10,0),
                "result" : 0
            },
            "2" : {
                "type" : 'Batsman',
                "runs" : getRandomValue(100, 30),
                "wicket" : getRandomValue(10,0),
                "result" : 1
            },
            "3" : {
                "type" : 'Batsman',
                "runs" : getRandomValue(100, 30),
                "wicket" : getRandomValue(10,0),
                "result" : 1
            }
        }
       },

       "2": {
        "name" : "Bumrah",
        "matches" : 3,
        "match_details" : {
            "1" : {
                "type" : 'Bowler',
                "runs" : getRandomValue(100, 30),
                "wicket" : 4,
                "result" : 1
            },
            "2" : {
                "type" : 'Bowler',
                "runs" : getRandomValue(100, 30),
                "wicket" : 5,
                "result" : 1
            },
            "3" : {
                "type" : 'Bowler',
                "runs" : getRandomValue(100, 30),
                "wicket" : 6,
                "result" : 1
            }
        }
       }

    }

}])